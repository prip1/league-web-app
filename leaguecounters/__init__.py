import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from os import path
from flask_login import LoginManager

load_dotenv() #include .env in environment variables

db = SQLAlchemy()
DB_NAME = "database.db"

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.getenv('SECRET_KEY') #grab environment variable names 'SECRET_KEY'
    app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}' #f character makes whatever is in {} to be evaluated as python string
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
    db.init_app(app)

    from .models import User, Summoner, Game
    from .views import views #relative import?
    from .auth import auth

    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(auth, url_prefix='/')

    create_database(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(id):
        return User.query.get(int(id))

    return app

def create_database(app):
    if not path.exists('website/' + DB_NAME):
        db.create_all(app=app)
        print('Created Database!')

# def initialize_database():
#     new_user = User(email='pat@yahoo.com', password='testpass123')
#     db.session.add(new_user)
#     new_summoner = Summoner(puuid='465345123', summoner_name='Platrick', summoner_level=28, user_id=1)
#     db.session.add(new_summoner)
#     new_game = Game(match_id='1000', win=True, role='Top', champion='Sion', enemy_champion='Singed', summoner_puuid='465345123')
#     new_game2 = Game(match_id='1001', win=True, role='Top', champion='Sion', enemy_champion='Mordekaiser', summoner_puuid='465345123')
#     db.session.add(new_game)
#     db.session.add(new_game2)
#     new_summoner = Summoner(puuid='7328591', summoner_name='Patrick214', summoner_level=35, user_id=1)
#     db.session.add(new_summoner)
#     new_game = Game(match_id='2893', win=True, role='Top', champion='Garen', enemy_champion='Aatrox', summoner_puuid='7328591')
#     db.session.add(new_game)
#     db.session.commit()
