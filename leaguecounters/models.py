from . import db # the . means "from package" - the website package we just created. Want to grab db from init, which is a part of the package
from flask_login import UserMixin #going to inherit some properties for our user object
from sqlalchemy.sql import func

user_summoner = db.Table('user_summoner',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('summoner_puuid', db.String(36), db.ForeignKey('summoner.puuid'))
)

class SummonerGame(db.Model):
    __tablename__ = "summoner_game"
    summoner_puuid = db.Column(db.String(36), db.ForeignKey('summoner.puuid'), primary_key = True)
    summoner_role = db.Column(db.String(6))
    summoner_champion = db.Column(db.String(30))
    summoner_team = db.Column(db.Integer)
    match_id = db.Column(db.String(14), db.ForeignKey('game.match_id'), primary_key = True)

#User will inherit from db.Model and UserMixin
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True) 
    email = db.Column(db.String(150), unique=True) #string with maximum length of 150 and email has to be unique
    password = db.Column(db.String(150))
    summoners = db.relationship('Summoner', secondary=user_summoner, backref="users")

    def __repr__(self):
        return f'<User: {self.id}>'

class Summoner(db.Model):
    puuid = db.Column(db.String(36), primary_key=True)
    summoner_name = db.Column(db.String(100))
    summoner_level = db.Column(db.Integer)
    games = db.relationship('Game', secondary='summoner_game', backref="summoners")

class Game(db.Model):
    match_id = db.Column(db.String(40), primary_key=True)
    winning_team = db.Column(db.Integer) #save as 1 or 2 for: team 100 and team 200

    