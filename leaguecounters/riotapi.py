
from .models import Summoner, User, user_summoner, Game, SummonerGame
import json
import requests
import os
import time
from . import db
from flask import flash
from flask_login import current_user

riotbaseurl = 'https://na1.api.riotgames.com/'
riotmatchurl = 'https://americas.api.riotgames.com/'
endpoint_name = 'lol/summoner/v4/summoners/by-name/'
endpoint_summoner_puuid = 'lol/summoner/v4/summoners/by-puuid/'
endpoint_games = 'lol/match/v5/matches/by-puuid/'
endpoint_match = 'lol/match/v5/matches/'
max_solo_queue = '&count=100&queue=420'


def get_summoner_by_name(name):
    #json.loads- 'json string' will be converted to python dictionary
    #json.dumps- python dictionary will be converted to json string
    #dont need to load .env because its loaded upon boot in init
    r = requests.get(riotbaseurl+endpoint_name+name+'?api_key='+os.getenv('RIOT_KEY'))
    print(riotbaseurl+endpoint_name+name+'?api_key='+os.getenv('RIOT_KEY'))
    data=json.loads(r.text)
    if(r.status_code !=200):
        data="invalid"

    return data

def load_games(puuid):
    #the point here is, we want to query the matches for a user, save the game info, and add every summoner in the game
    #this is to create a proper many to many relationship between Summoner and Game
    eof=False
    i=0
    while not eof:
        r = requests.get(riotmatchurl+endpoint_games+puuid+'/ids?api_key='+os.getenv('RIOT_KEY')+'&start='+str(i)+max_solo_queue)
        i=i+100
        if len(r.text) < 100: 
            eof=True
        for match in json.loads(r.text): #is this interrupt proof?
            match_exists = db.session.query(Game).filter(Game.match_id == match).first()
            check_partial_match_load = len(db.session.query(SummonerGame).filter(SummonerGame.match_id == match).all()) #should be length 10 if match is full loaded
            if(not match_exists):
                m = request_match_info(match)
                m = json.loads(m.text)
                print(m)
                #determine match winner
                if(m['info']['teams'][0]['teamId'] == 100):
                    winner = 100 if m['info']['teams'][0]['win'] == True else 200
                else:
                    winner = 200 if m['info']['teams'][0]['win'] == True else 100

                new_game = Game(match_id = m['metadata']['matchId'], winning_team = winner)
                db.session.add(new_game)
                db.session.commit()
            
                for s in m['metadata']['participants']:
                    #add the summoners from this game into our db
                    summoner = db.session.query(Summoner).filter(Summoner.puuid == s).first()
                    game_already_added = 0
                    if(summoner):
                        for game in summoner.games:
                            if game.match_id == match:                                
                                game_already_added = 1
                        if(not game_already_added):
                            summoner.games.append(new_game) #add summoner role etc here
                            db.session.commit()
                            sum_info = db.session.query(SummonerGame).filter(SummonerGame.summoner_puuid == summoner.puuid, SummonerGame.match_id == match).first()
                            for player in m['info']['participants']:
                                if player['puuid'] == sum_info.summoner_puuid:
                                    print("it worked")
                                    #sum_info.summoner_role = player['lane']
                                    sum_info.summoner_role = player['teamPosition']
                                    sum_info.summoner_champion = player['championName']
                                    sum_info.summoner_team = player['teamId']
                                    db.session.add(sum_info)
                                    db.session.commit() 
                            db.session.commit() 
                    else:
                        print(s)
                        sum = get_summoner_by_puuid(s)
                        add_summoner_by_response_ignore_user(sum)
                        sum = db.session.query(Summoner).filter(Summoner.puuid == s).first()
                        sum.games.append(new_game)
                        sum_info = db.session.query(SummonerGame).filter(SummonerGame.summoner_puuid == sum.puuid).first()
                        print("Summoner info:")
                        
                        for player in m['info']['participants']:
                            if player['puuid'] == sum_info.summoner_puuid:
                                sum_info.summoner_role = player['teamPosition']
                                sum_info.summoner_champion = player['championName']
                                sum_info.summoner_team = player['teamId']
                                db.session.add(sum_info)
                                db.session.commit()
                         
                        print('added game to summoner')
                        time.sleep(2)
                time.sleep(2)
            else:
                print(check_partial_match_load)
                if (check_partial_match_load > 0) and (check_partial_match_load < 10):
                    m = request_match_info(match)
                    m = json.loads(m.text)
                    time.sleep(2)
                    #this is a partial load, needs to be finished
                    for s in m['info']['participants']:
                        if db.session.query(Summoner).filter(Summoner.puuid == s['puuid']).first() not in match_exists.summoners:
                            #add summoner to game
                            new_summoner = get_summoner_by_puuid(s['puuid'])
                            add_summoner_by_response_ignore_user(new_summoner)
                            new_summoner = db.session.query(Summoner).filter(Summoner.puuid == s['puuid']).first()
                            match_exists.summoners.append(new_summoner)
                            sum_info = db.session.query(SummonerGame).filter(SummonerGame.summoner_puuid == new_summoner.puuid).first()
                            sum_info.summoner_role = s['teamPosition']
                            sum_info.summoner_champion = s['championName']
                            sum_info.summoner_team = s['teamId']
                            db.session.add(sum_info)
                            db.session.commit()
                            

        

def add_summoner_by_response(s):
    summoners = db.session.query(Summoner).filter(Summoner.puuid == s['puuid']).all() #first determine if the summoner is in our db. summoners == 0 if the summoner doesnt exist. if it does exist, we must determine if its been added for current user 
    exists=0
    for summoner in summoners:
        for user in summoner.users:
            if(user.id == current_user.id):
                exists=1 #combination exists, maybe break here?
    if(summoners and exists):
        flash('Summoner already exists.', category='error') #summoner and user email combination already exist
    elif(summoners):   
        current_user.summoners.append(summoner) #summoner exists but user needs to be added to relationship
        db.session.commit()      
        flash('Summoner added.', category='success')
    else:    
        new_summoner = Summoner(puuid = s['puuid'], summoner_name = s['name'], summoner_level = s['summonerLevel']) #need to create new summoner and add the current user
        db.session.add(new_summoner)
        current_user.summoners.append(new_summoner)
        db.session.commit()
        flash('Summoner added.', category='success')

def add_summoner_by_response_ignore_user(s):
    summoners = db.session.query(Summoner).filter(Summoner.puuid == s['puuid']).all() #first determine if the summoner is in our db. summoners == 0 if the summoner doesnt exist. if it does exist, we must determine if its been added for current user 
    if(summoners):
        print("Summoner Exists") #summoner and user email combination already exist
    else:    
        new_summoner = Summoner(puuid = s['puuid'], summoner_name = s['name'], summoner_level = s['summonerLevel']) #need to create new summoner and add the current user
        db.session.add(new_summoner)
        db.session.commit()
        print('summoner added')

def request_match_info(match_id):
    r = requests.get(riotmatchurl+endpoint_match+match_id+'/?api_key='+os.getenv('RIOT_KEY'))
    print(riotmatchurl+endpoint_match+match_id+'/ids?api_key='+os.getenv('RIOT_KEY'))
    return r

def get_summoner_by_puuid(puuid):
    r = requests.get(riotbaseurl+endpoint_summoner_puuid+puuid+'?api_key='+os.getenv('RIOT_KEY'))
    data=json.loads(r.text)
    return data

champion_list = [
    "Aatrox",
    "Ahri",
    "Akali",
    "Akshan",
    "Alistar",
    "Amumu",
    "Anivia",
    "Annie",
    "Aphelios",
    "Ashe",
    "AurelionSol",
    "Azir",
    "Bard",
    "Blitzcrank",
    "Brand",
    "Braum",
    "Caitlyn",
    "Camille",
    "Cassiopeia",
    "Chogath",
    "Corki",
    "Darius",
    "Diana",
    "Draven",
    "DrMundo",
    "Ekko",
    "Elise",
    "Evelynn",
    "Ezreal",
    "Fiddlesticks",
    "Fiora",
    "Fizz",
    "Galio",
    "Gangplank",
    "Garen",
    "Gnar",
    "Gragas",
    "Graves",
    "Gwen",
    "Hecarim",
    "Heimerdinger",
    "Illaoi",
    "Irelia",
    "Ivern",
    "Janna",
    "JarvanIV",
    "Jax",
    "Jayce",
    "Jhin",
    "Jinx",
    "Kaisa",
    "Kalista",
    "Karma",
    "Karthus",
    "Kassadin",
    "Katarina",
    "Kayle",
    "Kayn",
    "Kennen",
    "Khazix",
    "Kindred",
    "Kled",
    "KogMaw",
    "Leblanc",
    "LeeSin",
    "Leona",
    "Lillia",
    "Lissandra",
    "Lucian",
    "Lulu",
    "Lux",
    "Malphite",
    "Malzahar",
    "Maokai",
    "MasterYi",
    "MissFortune",
    "MonkeyKing",
    "Mordekaiser",
    "Morgana",
    "Nami",
    "Nasus",
    "Nautilus",
    "Neeko",
    "Nidalee",
    "Nocturne",
    "Nunu",
    "Olaf",
    "Orianna",
    "Ornn",
    "Pantheon",
    "Poppy",
    "Pyke",
    "Qiyana",
    "Quinn",
    "Rakan",
    "Rammus",
    "RekSai",
    "Rell",
    "Renata",
    "Renekton",
    "Rengar",
    "Riven",
    "Rumble",
    "Ryze",
    "Samira",
    "Sejuani",
    "Senna",
    "Seraphine",
    "Sett",
    "Shaco",
    "Shen",
    "Shyvana",
    "Singed",
    "Sion",
    "Sivir",
    "Skarner",
    "Sona",
    "Soraka",
    "Swain",
    "Sylas",
    "Syndra",
    "TahmKench",
    "Taliyah",
    "Talon",
    "Taric",
    "Teemo",
    "Thresh",
    "Tristana",
    "Trundle",
    "Tryndamere",
    "TwistedFate",
    "Twitch",
    "Udyr",
    "Urgot",
    "Varus",
    "Vayne",
    "Veigar",
    "Velkoz",
    "Vex",
    "Vi",
    "Viego",
    "Viktor",
    "Vladimir",
    "Volibear",
    "Warwick",
    "Xayah",
    "Xerath",
    "XinZhao",
    "Yasuo",
    "Yone",
    "Yorick",
    "Yuumi",
    "Zac",
    "Zed",
    "Zeri",
    "Ziggs",
    "Zilean",
    "Zoe",
    "Zyra"
]

champion_winrates = {
    "Aatrox" : 0,
    "Ahri" : 0,
    "Akali" : 0,
    "Akshan" : 0,
    "Alistar" : 0,
    "Amumu" : 0,
    "Anivia" : 0,
    "Annie" : 0,
    "Aphelios" : 0,
    "Ashe" : 0,
    "AurelionSol" : 0,
    "Azir" : 0,
    "Bard" : 0,
    "Blitzcrank" : 0,
    "Brand" : 0,
    "Braum" : 0,
    "Caitlyn" : 0,
    "Camille" : 0,
    "Cassiopeia" : 0,
    "Chogath" : 0,
    "Corki" : 0,
    "Darius" : 0,
    "Diana" : 0,
    "Draven" : 0,
    "DrMundo" : 0,
    "Ekko" : 0,
    "Elise" : 0,
    "Evelynn" : 0,
    "Ezreal" : 0,
    "Fiddlesticks" : 0,
    "Fiora" : 0,
    "Fizz" : 0,
    "Galio" : 0,
    "Gangplank" : 0,
    "Garen" : 0,
    "Gnar" : 0,
    "Gragas" : 0,
    "Graves" : 0,
    "Gwen" : 0,
    "Hecarim" : 0,
    "Heimerdinger" : 0,
    "Illaoi" : 0,
    "Irelia" : 0,
    "Ivern" : 0,
    "Janna" : 0,
    "JarvanIV" : 0,
    "Jax" : 0,
    "Jayce" : 0,
    "Jhin" : 0,
    "Jinx" : 0,
    "Kaisa" : 0,
    "Kalista" : 0,
    "Karma" : 0,
    "Karthus" : 0,
    "Kassadin" : 0,
    "Katarina" : 0,
    "Kayle" : 0,
    "Kayn" : 0,
    "Kennen" : 0,
    "Khazix" : 0,
    "Kindred" : 0,
    "Kled" : 0,
    "KogMaw" : 0,
    "Leblanc" : 0,
    "LeeSin" : 0,
    "Leona" : 0,
    "Lillia" : 0,
    "Lissandra" : 0,
    "Lucian" : 0,
    "Lulu" : 0,
    "Lux" : 0,
    "Malphite" : 0,
    "Malzahar" : 0,
    "Maokai" : 0,
    "MasterYi" : 0,
    "MissFortune" : 0,
    "MonkeyKing" : 0,
    "Mordekaiser" : 0,
    "Morgana" : 0,
    "Nami" : 0,
    "Nasus" : 0,
    "Nautilus" : 0,
    "Neeko" : 0,
    "Nidalee" : 0,
    "Nocturne" : 0,
    "Nunu" : 0,
    "Olaf" : 0,
    "Orianna" : 0,
    "Ornn" : 0,
    "Pantheon" : 0,
    "Poppy" : 0,
    "Pyke" : 0,
    "Qiyana" : 0,
    "Quinn" : 0,
    "Rakan" : 0,
    "Rammus" : 0,
    "RekSai" : 0,
    "Rell" : 0,
    "Renata" : 0,
    "Renekton" : 0,
    "Rengar" : 0,
    "Riven" : 0,
    "Rumble" : 0,
    "Ryze" : 0,
    "Samira" : 0,
    "Sejuani" : 0,
    "Senna" : 0,
    "Seraphine" : 0,
    "Sett" : 0,
    "Shaco" : 0,
    "Shen" : 0,
    "Shyvana" : 0,
    "Singed" : 0,
    "Sion" : 0,
    "Sivir" : 0,
    "Skarner" : 0,
    "Sona" : 0,
    "Soraka" : 0,
    "Swain" : 0,
    "Sylas" : 0,
    "Syndra" : 0,
    "TahmKench" : 0,
    "Taliyah" : 0,
    "Talon" : 0,
    "Taric" : 0,
    "Teemo" : 0,
    "Thresh" : 0,
    "Tristana" : 0,
    "Trundle" : 0,
    "Tryndamere" : 0,
    "TwistedFate" : 0,
    "Twitch" : 0,
    "Udyr" : 0,
    "Urgot" : 0,
    "Varus" : 0,
    "Vayne" : 0,
    "Veigar" : 0,
    "Velkoz" : 0,
    "Vex" : 0,
    "Vi" : 0,
    "Viego" : 0,
    "Viktor" : 0,
    "Vladimir" : 0,
    "Volibear" : 0,
    "Warwick" : 0,
    "Xayah" : 0,
    "Xerath" : 0,
    "XinZhao" : 0,
    "Yasuo" : 0,
    "Yone" : 0,
    "Yorick" : 0,
    "Yuumi" : 0,
    "Zac" : 0,
    "Zed" : 0,
    "Zeri" : 0,
    "Ziggs" : 0,
    "Zilean" : 0,
    "Zoe" : 0,
    "Zyra": 0
}

