from flask import Blueprint, render_template, request, flash, jsonify
from flask_login import login_required, current_user
from .models import Summoner, User, user_summoner, Game, SummonerGame
from . import db
from sqlalchemy import func
from .riotapi import load_games, get_summoner_by_name, add_summoner_by_response, champion_list, champion_winrates




views = Blueprint('views', __name__)

@views.route('/', methods=['POST','GET'])
@login_required
def home():
    print(len(db.session.query(Game).all()))
    if request.method == 'POST':
        if "load" in request.form:
            load_games(request.form.get('load'))
        if "delete-id" in request.form:
            d = request.form.get('delete-id')
            current_user.summoners.remove(db.session.query(Summoner).filter(Summoner.puuid == d).first())

        db.session.commit()

    return render_template('home.html', user=current_user)

@views.route('/update', methods=['GET','POST'])
@login_required
def update():
    if request.method == 'POST':
        name = request.form.get('summoner')    
        s = get_summoner_by_name(name)
        if(s == 'invalid'):
            flash('Please enter valid Summoner Name', category='error')
        else:    
            add_summoner_by_response(s)
        

    return render_template('update.html', user=current_user)

@views.route('/counters', methods=['GET','POST'])
@login_required
def counters():
    sorted_winrates = []
    trimmed_winrates = {}
    counters = []
    if request.method == 'POST':
        enemy = request.form.get('enemy_lane')
        #Grab all games for all summoners attached to a user
        user_games = db.session.query(User, Summoner, Game).join(Summoner, User.summoners).join(Game, Summoner.games).filter(User.id == current_user.id).all()
        print(len(user_games))
        total_games = 0 
        counters = CounterMatchups(enemy)
        for user, summoner, game in user_games:
            total_games += 1
            for s in current_user.summoners:
                if s in game.summoners:
                    friendly_summoner_game = db.session.query(SummonerGame).filter(SummonerGame.summoner_puuid == s.puuid, SummonerGame.match_id == game.match_id).first() #get the match info for user summoner game instance
                    temp_summoner_game = db.session.query(SummonerGame).filter(SummonerGame.match_id == game.match_id, SummonerGame.summoner_role == friendly_summoner_game.summoner_role).all() #grab both, eliminate the one we dont want (friendly)
                    for s in temp_summoner_game:
                        if s.summoner_puuid != friendly_summoner_game.summoner_puuid:
                            enemy_summoner_game = s

                    #determine if we laned against desired champ
                    if enemy_summoner_game.summoner_champion == enemy:
                        friendly_summoner_champion = friendly_summoner_game.summoner_champion

                        # print("Friendly Team: " + str(friendly_summoner_game.summoner_team))
                        # print("Champion: " + str(friendly_summoner_game.summoner_champion))
                        # print(total_games)
                        # print(friendly_summoner_game.match_id)
                        if(friendly_summoner_game.summoner_team == game.winning_team):
                            #good experience figuring out how to call this function
                            getattr(counters, friendly_summoner_champion).add_win()
                        else:
                            getattr(counters, friendly_summoner_champion).add_loss()
        
        for champ in champion_list:
            winrate = getattr(counters, champ).get_winrate()
            champion_winrates[champ] = winrate
        
        #sort by highest wr to lowest
        sorted_winrates = {k: v for k, v in sorted(champion_winrates.items(), key = lambda v:v[1], reverse = True)}

        #lets trim this to remove champs who didnt have games, convert to percent form, round decimals
        for champ in sorted_winrates:
            if getattr(counters, champ).get_winrate() != -1:
                trimmed_winrates[champ] = round(((getattr(counters, champ).get_winrate()) * 100), 1)
        
        for champ in sorted_winrates:
            if sorted_winrates[champ] != -1:
                print("Winrate as " + champ + ": " + str(sorted_winrates[champ]))
                print("Wins: " + str(getattr(counters, champ).wins))
                print("Losses: " + str(getattr(counters, champ).losses))
                print("")

        # for champ in champion_list:
        #     print("Champion: " + champ)
        #     print("Wins: " + str(getattr(counters, champ).wins))
        #     print("Losses: " + str(getattr(counters, champ).losses))
        #     print("Winrate: " + str(champion_winrates[champ]))

        # print("Total Games: " + str(total_games))               
        # print("sion wins: " + str(counters.Sion.wins))    
        # print("sion losses: " + str(counters.Sion.losses))  
        # print("Irelia wins: " + str(counters.Irelia.wins))    
        # print("Irelia losses: " + str(counters.Irelia.losses))          
        # for game, summoner in db.session.query(Game, SummonerGame).join(SummonerGame, Game.match_id == SummonerGame.match_id).filter(SummonerGame.summoner_champion == enemy).all():
        #     print(str(game.winning_team) + " " + summoner.summoner_role)

    return render_template("counters.html", user=current_user, winrates=trimmed_winrates, counter_list = counters)

@views.route('/index', methods=['GET','POST'])
@login_required
def index():
    return render_template('index.html')

class CounterMatchups:
    number_of_games = 0
    def __init__(self, enemy_lane):
        self.enemy_lane = enemy_lane
        self.initialize_matchups()

    def initialize_matchups(self):
        for champ in champion_list:
            setattr(self, champ, Matchup(champ))

class Matchup:
    def __init__(self, name):
        self.name = name
        self.wins = 0
        self.losses = 0
    
    def add_win(self):
        self.wins += 1
    
    def add_loss(self):
        self.losses += 1
    
    def get_winrate(self):
        if (self.wins+self.losses) > 0:
            return self.wins/(self.wins+self.losses)
        else:
            return -1
